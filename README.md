Description
===========
Pause Nuke's Viewer nodes to prevent any image processing.

This is useful to prevent Nuke from processing the image when loading a Nuke
Script in order to keep interacting with the working file quick while not
deleting any Viewer node or changing/ un-piping any node connection.

Nuke doesn't offer the functionality to access the 'Viewer Pause' button
directly. In addition, it doesn't let us access nuke.activeViewer() when using
an onLoad callback. For that reason we use some kind of workaround to prevent
Nuke from processing the current image by setting all active viewer inputs to
None. This feels actually quite a bit like a hack, but it works smoothly and
reliable.

Installation
------------
Put the whole pause_viewer into your Nuke home direcory or in any other
location of your NUKE_PATH. Add the following line to your init.py:

nuke.pluginAddPath("pause_viewer")
