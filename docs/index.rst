Welcome to pause_viewer's documentation!
========================================

pause_viewer provides functionality to pause Nuke's Viewer nodes to prevent any
image processing. This is useful to prevent Nuke from processing the image when
loading a Nuke Script in order to keep interacting with the working file quick
while not deleting any Viewer node or changing/ un-piping any node connection.

Nuke doesn't offer the functionality to access the 'Viewer Pause' button
directly. In addition, it doesn't let us access nuke.activeViewer() when using
an onLoad callback. For that reason we use some kind of workaround to prevent
Nuke from processing the current image by setting all active viewer inputs to
``None``. This feels actually quite a bit like a hack, but it works smoothly
and reliable.


.. toctree::
   :maxdepth: 2

   download_installation
   usage
   credits_feedback
   changelog

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
