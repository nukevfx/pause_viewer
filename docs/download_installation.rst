.. _installation:

Download
========
You can download pause_viewer from:

`<http://www.leafpictures.de/>`_

`<http://www.nukepedia.com/>`_

Or clone it directly from ``git@gitlab.com:nukevfx/pause_viewer.git``

Installation
------------
Put the whole **pause_viewer** into your Nuke home directory or in any other
location of your ``NUKE_PATH``. Add the following line to your init.py::

    nuke.pluginAddPath("pause_viewer")
