.. _usage:

Usage
=====
pause_viewer activates two things:

    - An onLoad callback.
    - A command to execute the pause function manually.

The onLoad callback
-------------------
The onLoad callback will make sure to execute the pause functionality when
a Nuke script is loaded. This will prevent Nuke from processing the image.

.. image:: _static/dag.jpg

The command
-----------
Nuke provides the shortcut **P** to pause the Viewer. However, this shortcut
is context sensitive to the Viewer node. Pressing **P** in the DAG creates a
RotoPaint node. To access the pause viewer functionality everywhere you can
press **Ctrl+Y** (Linux, Windows) / **cmd + Y** (Mac). The command is also
accessible from Nuke's menu bar as seen below.

.. image:: _static/ui_command.jpg

In order to re-activate your viewer you can simply press one of your numbers on
your number pad.
