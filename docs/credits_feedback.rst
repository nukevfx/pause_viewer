.. _credits_feedback:

Credits
=======
**Simon Jokuschies**

Personal wesbite
`<http://www.leafpictures.de/>`_

cragl vfx tools
`<http://www.cragl.com/>`_

Feedback
--------
Feedback is always welcome. Please send it to info@leafpictures.de
