"""Test core."""

# Import built-in modules
import unittest

# Import third-party modules
import nuke  # pylint: disable=import-error

# Import local modules
from pause_viewer import core


# We want to have long and descriptive test names.
# pylint: disable=invalid-name
class TestCore(unittest.TestCase):
    """Test core."""

    def setUp(self):
        """Set up test."""
        self.all_nodes = nuke.allNodes()

        self.checkerboard = nuke.createNode("CheckerBoard2")
        self.viewer = nuke.createNode("Viewer")

        nuke.activeViewer().activateInput(0)

    def tearDown(self):
        """Clean up after test."""
        test_nodes = [node for node in nuke.allNodes()
                      if node not in self.all_nodes]

        for node in test_nodes:
            nuke.delete(node)

    def test_active_viewer_is_not_none(self):
        """The active viewer should be set to 0 which is the first input."""
        self.assertEqual(0, nuke.activeViewer().activeInput())

    def test_set_active_viewer_none(self):
        """Ensure the active input is set to None."""
        core.pause_viewer(self.viewer)
        self.assertIsNone(nuke.activeViewer().activeInput())

    def test_keep_unmodified_unchanged(self):
        """Ensure that the unmodified state stays in tact."""
        nuke.root().setModified(False)
        self.assertFalse(nuke.root().modified())

        core.pause_viewers()
        self.assertFalse(nuke.root().modified())

    def test_keep_modified_unchanged(self):
        """Ensure that the unmodified state stays in tact."""
        nuke.root().setModified(True)
        self.assertTrue(nuke.root().modified())

        core.pause_viewers()
        self.assertTrue(nuke.root().modified())


def main():
    """Run all unittests."""
    unittest.main()


nuke.executeInMainThread(main)
