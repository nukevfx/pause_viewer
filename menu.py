"""Nuke menu py to add the commands and callbacks to Nuke."""

# Import third-party modules
import nuke  # pylint: disable=import-error

# Import local modules
from pause_viewer import core


def init():
    """Initialize the ui elements and callbacks."""
    # Add the pause viewers command.
    viewer_menu = nuke.menu("Nuke").findItem("Viewer")
    viewer_menu.addCommand("Pause Viewer", core.pause_viewers, "Ctrl+Y")

    # Add onScriptLoad callback to pause the viewers.
    nuke.addOnScriptLoad(core.pause_viewers)


init()
