"""Pause the viewers to prevent any image processing.

This is useful to prevent Nuke from processing the image when loading a Nuke
Script in order to keep interacting with the working file quick while not
deleting any Viewer node or changing/ un-piping any node connection.

Nuke doesn't offer the functionality to access the 'Viewer Pause' button
directly. In addition, it doesn't let us access nuke.activeViewer() when using
an onLoad callback. For that reason we use some kind of workaround to prevent
Nuke from processing the current image by setting all active viewer inputs to
None. This feels actually quite a bit like a hack, but it works smoothly and
reliable.

"""

# Import third-party modules
import nuke  # pylint: disable=import-error
import nukescripts  # pylint: disable=import-error


def pause_viewer(viewer):
    """Pause given Viewer node by setting the active input to None.

    Args:
        viewer (nuke.Node): The viewer to pause from any image processing.

    """
    index = viewer.inputs() + 1
    dot = nuke.nodes.Dot()
    dot.selectOnly()
    nukescripts.connect_selected_to_viewer(index)
    nuke.delete(dot)


def pause_viewers():
    """Pause all Viewer nodes from processing the current image.

    Accessing the Pause button is not provided by Nuke. Accessing the active
    Viewer is also not available when using an onLoad callback. For that we do
    some kind of workaround in here: Iterate over all Viewer, get the next
    available viewer input and connect it with a Dot node which we will then
    remove. By doing so, Nuke will not have an active Viewer input anymore.
    As nuke.activeViewer() is not available in an onLoad callback, we need to
    iterate through all Viewer nodes to ensure that nothing gets processed.

    As this workaround create and deletes temporary nodes, it changes the
    modify state of the nuke.root. If the state is unmodified, we should keep
    this as-is.

    """
    modified_before = nuke.root().modified()

    for viewer in nuke.allNodes("Viewer"):
        pause_viewer(viewer)

    if not modified_before and nuke.root().modified():
        nuke.root().setModified(False)
